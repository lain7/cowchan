#!/usr/bin/env python3
"""
cowchan
Copyright (C) 2018  lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import json, urllib.request , random, re, codecs
from sys import argv,exit
VERSION = "1.0.0"
def cleanhtml(html):
  #cleans the quote of unneeded html
  quote = re.sub(re.compile('<a.*?>'), '', html)
  quote = re.sub(re.compile('</a>'), '', quote)
  quote = re.sub(re.compile('<span.*?>'), '', quote)
  quote = re.sub(re.compile('</span>'), '', quote)
  quote = re.sub(re.compile('<pre.*?>'), '', quote)
  quote = re.sub(re.compile('</pre>'), '', quote)
  quote = re.sub(re.compile('</s>'), '', quote)
  quote = re.sub(re.compile('<s>'), '', quote)
  quote = re.sub(re.compile('<wbr>'), '', quote)
  return(quote)

def getQuote(board):
  # if a post does not have any text it will not have a ["com"]
  # key and will crash the program
  # with this we just run it again until we get a valid ["com"]
  # as of 2017-08-02 this works without crash
  failsafe = 0
  while(True):
    request = urllib.request.Request("https://a.4cdn.org/" + board + "/threads.json" , None, {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7', })
    threads = json.loads(urllib.request.urlopen(request).read().decode("UTF-8"))
    #the json from the api is lists of dictionaries which makes this look messy
    threadPageNum = random.randint(0 ,len(threads)-1)
    threadPostNum = random.randint(0 ,len(threads[threadPageNum]["threads"])-1)
    parentPostNumber = threads[threadPageNum]["threads"][threadPostNum]["no"]
    request = urllib.request.Request("https://a.4cdn.org/"+ board + "/thread/" + str(parentPostNumber) + ".json", None, {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7', })
    posts = json.loads(urllib.request.urlopen(request).read().decode("UTF-8"))
    postNumber = random.randint(0, len(posts["posts"])-1)
    if("com" in posts["posts"][postNumber]):
      break
    elif(failsafe == 15):
      print("Could not find post with [\"com\"]\n")
      print("This could be the result of an api error or an unpopulated board")
      exit(0)
    else:
      failsafe += 1
  quote = cleanhtml(posts["posts"][postNumber]["com"])
  #clean up the quote
  quote = quote.replace("&gt;&gt;", "\n>>")
  quote = quote.replace("&gt;", "\n>")
  quote = quote.replace("&#039;", "'")
  quote = quote.replace("&quot;", "\"")
  quote = quote.replace("<br>", "\n")
  quote = quote.replace("\n", '', 1)
  return(quote)

def main():
  if("-v" in argv or "--version" in argv):
    print("cowchan " + VERSION)
    return(0)
  elif (len(argv) == 2 or "-h" in argv or "--help" in argv or ('-b' not in argv and "--board" not in argv)):
    print("Usage: ./cowchan.py --board board1 board2...\n")
    print("Argument         Function                           Example")
    print("--board or -b   | Specifies which board(s) to use  | -b po mu wsg")
    print("--help or -h    | Displays this help text          | -h")
    print("--version or -v | Shows the version of the program | -v")
    return(0)
  enabledBoards = []
  for i in range(2,len(argv)):
    enabledBoards.append(argv[i])
  board = random.choice(enabledBoards)
  print(getQuote(board))
if __name__ == "__main__":
  main()
