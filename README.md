cowchan

A program that prints a random 4chan post from a board or the boards of your
choice.
This program is meant to be piped to something like cowsay.

![](cowchan.png)

- Dependencies:
  - python3

Usage: './cowchan.py arguments...'

|    Argument     |                  Function                  |   Example    |
|-----------------|--------------------------------------------|--------------|
|  --board or -b  | Specifies which boards to get a quote from | -b po mu wsg |
|  --help or -h   | Displays help text                         | -h           |
| --version or -v | Displays the version of the program        | -v           |


 Version 1.0.0 
